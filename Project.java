import java.util.*;

public class Project {

	static int[][] currentGrid = new int[4][4];
	
	public Project() {
		
	}
	
	public int[] shuffle() {	// creates a random order with which to shuffle the ints (helper function)
		ArrayList<Integer> orig = new ArrayList<>(16);
		orig.add(0);
		orig.add(1);
		orig.add(2);
		orig.add(3);
		orig.add(4);
		orig.add(5);
		orig.add(6);
		orig.add(7);
		orig.add(8);
		orig.add(9);
		orig.add(10);
		orig.add(11);
		orig.add(12);
		orig.add(13);
		orig.add(14);
		orig.add(15);
		
		ArrayList<Integer> shuffled = new ArrayList<>(16);
		while(orig.size() > 0) {
			int k = (int) (Math.random() * orig.size());
			int origK = orig.get(k);
			shuffled.add(origK);
			orig.remove(k);
		}
		
		int[] shuffledArray = new int[16];
		for(int i = 0; i < 16; i++) {
			shuffledArray[i] = shuffled.get(i);
		}
		return shuffledArray;
	}
	
	public static int[][] answerGrid() {	// each time the player makes a move, compare to this (if equal, player wins)
		int[][] answerGrid = new int[4][4];
		answerGrid[0][0] = 1;
		answerGrid[0][1] = 2;
		answerGrid[0][2] = 3;
		answerGrid[0][3] = 4;
		answerGrid[1][0] = 5;
		answerGrid[1][1] = 6;
		answerGrid[1][2] = 7;
		answerGrid[1][3] = 8;
		answerGrid[2][0] = 9;
		answerGrid[2][1] = 10;
		answerGrid[2][2] = 11;
		answerGrid[2][3] = 12;
		answerGrid[3][0] = 13;
		answerGrid[3][1] = 14;
		answerGrid[3][2] = 15;
		answerGrid[3][3] = 0;
		return answerGrid;
	}
	
	public void startGrid() {	// this is the (shuffled) starting board for the players
		int[] order = shuffle();
		currentGrid[0][0] = order[0];
		currentGrid[0][1] = order[1];
		currentGrid[0][2] = order[2];
		currentGrid[0][3] = order[3];
		currentGrid[1][0] = order[4];
		currentGrid[1][1] = order[5];
		currentGrid[1][2] = order[6];
		currentGrid[1][3] = order[7];
		currentGrid[2][0] = order[8];
		currentGrid[2][1] = order[9];
		currentGrid[2][2] = order[10];
		currentGrid[2][3] = order[11];
		currentGrid[3][0] = order[12];
		currentGrid[3][1] = order[13];
		currentGrid[3][2] = order[14];
		currentGrid[3][3] = order[15];
	}
	
	public static void draw(int[][] grid) {	// when you call this method, use the result of writeGrid() as params
		System.out.println("---------------------");
		for(int i = 0; i < 4; i++) {
			// print each row (i.e. grid[i][whatever])
			for(int j = 0; j < 4; j++) {
				String toPrint = grid[i][j] + " ";
				// print each number in the row (i.e. grid[i][j])
				if(grid[i][j] < 10) {
					toPrint = " " + toPrint;
				}
				if(grid[i][j] == 0) {
					toPrint = "   ";
				}
				System.out.print("| " + toPrint);
			}
			System.out.println("|\n---------------------");
		}
	}
	
	public boolean valid(String move) {
		int[] zeroCo = findZero();
		switch(move) {
			case "w": if(zeroCo[0] == 3) return false;
					  break;
			case "a": if(zeroCo[1] == 3) return false;
					  break;
			case "s": if(zeroCo[0] == 0) return false;
					  break;
			case "d": if(zeroCo[1] == 0) return false;
					  break;
		}
		return true;
	}
	
	public void up() {
		int[] zeroCo = findZero();
		int temp = currentGrid[zeroCo[0] + 1][zeroCo[1]];
		currentGrid[zeroCo[0]][zeroCo[1]] = temp;
		currentGrid[zeroCo[0] + 1][zeroCo[1]] = 0;
	}
	
	public void left() {
		int[] zeroCo = findZero();
		int temp = currentGrid[zeroCo[0]][zeroCo[1] + 1];
		currentGrid[zeroCo[0]][zeroCo[1]] = temp;
		currentGrid[zeroCo[0]][zeroCo[1] + 1] = 0;
	}
	
	public void down() {
		int[] zeroCo = findZero();
		int temp = currentGrid[zeroCo[0] - 1][zeroCo[1]];
		currentGrid[zeroCo[0]][zeroCo[1]] = temp;
		currentGrid[zeroCo[0] - 1][zeroCo[1]] = 0;
	}
	
	public void right() {
		int[] zeroCo = findZero();
		int temp = currentGrid[zeroCo[0]][zeroCo[1] - 1];
		currentGrid[zeroCo[0]][zeroCo[1]] = temp;
		currentGrid[zeroCo[0]][zeroCo[1] - 1] = 0;
	}
	
	public int[] findZero() {
		// return 2-item array containing i and j coordinates of 0 / the empty square in the grid
		int[] coord = new int[2];
		for(int i = 3; i >= 0; i--) {
			for(int j = 3; j >= 0; j--) {
				if(currentGrid[i][j] == 0) {
					coord[0] = i;
					coord[1] = j;
				}
			}
		}
		return coord;
	}
	
	public static void main(String[] args) {
		System.out.println("Welcome to the 15-Puzzle!\nMove the tiles with WASD to put them in order.");
		int counter = 0;
		Project grid = new Project();
		grid.startGrid();
		draw(currentGrid);
		System.out.println("Moves: " + counter);
		Scanner scanner = new Scanner(System.in);
		while(!currentGrid.equals(answerGrid())) {
			// user controls with WASD
			String move = scanner.nextLine();
			if(grid.valid(move)) {
				if(move.equals("w")) {
					grid.up();
					counter++;
				} else if(move.equals("a")) {
					grid.left();
					counter++;
				} else if(move.equals("s")) {
					grid.down();
					counter++;
				} else if(move.equals("d")) {
					grid.right();
					counter++;
				}
				draw(currentGrid);
				System.out.println("Moves: " + counter);
			}
		}
		System.out.println("You won!");
		scanner.close();
	}

}
